# git-template
This is an example modern template for an organized git project.

## Why
The idea behind this template is to get rid of unique tags and instead use branches for version keeping. The benefits of using versioned branches include:
+   Having a full unique history for each version/important stage of the development process
+   Better project structure enforcement
+   Better project organization with the possiblility of clear local maintainers of named branch versions as their names will always be on the branches they maintain (unless it's a globally maintained branch such as version/\*.\*.\*/release/\* or feature/\*/patch/current/\*)
+   And more

## Protected Branches
Essentially these ideas are enforced by protected branches.

*D = developers, M = maintainers*
| Branch Pattern                    	| Can Merge 	| Can Push 	|
|-----------------------------------	|-----------	|----------	|
| current                           	| M         	|          	|
| feature/\*/patch/\*/\*            	| M         	| D        	|
| feature/\*/patch/current       	    | M         	| M        	|
| feature/\*/release/\*             	|           	| M        	|
| version/\*.\*.\*/patch/\*/\*      	| M         	| D        	|
| version/\*.\*.\*/patch/current    	| M         	| M        	|
| version/\*.\*.\*/release/\*       	|           	| M        	|

## Example Usage (Merge/Rebase Cycle)
```markdown
(from *feature/update-readme/patch/current*)
->  *feature/update-readme/patch/jadelynnmasker/0*
    +   corrected some spelling mistakes
    +   made it more fun to read
(from *feature/update-readme/patch/current*)
->  *feature/update-readme/patch/jadelynnmasker/1*
    +   merge changes from *feature/update-readme/patch/jadelynnmasker/0*
    +   grammar fixes
->  *feature/update-readme/patch/current*
->  *feature/update-readme/release/0*
->  *version/0.1.1/patch/current*
->  *version/0.1.1/release/0*

later that same day...

(from *feature/update-readme/release/0*)
->  *feature/update-readme/patch/current*
->  *feature/update-readme/patch/someone-else/0*
    +   removed super secret
->  *feature/update-readme/patch/current*
    +   fix formatting
->  *feature/update-readme/release/1*
->  *version/0.1.1/patch/current*
->  *version/0.1.1/release/1*

soon after...

(from *version/0.1.1/release/1*)
->  *version/0.1.1/patch/jadelynnmasker/add-contributer-to-contribution-list*
    +   add someone-else to CONTRIBUTORS.md
->  *version/0.1.1/patch/current*
->  *version/0.1.1/release/2*
(from *version/0.1.1/patch/jadelynnmasker/add-contributor-to-contribution-list*)
->  *version/0.1.2/patch/current*
```
